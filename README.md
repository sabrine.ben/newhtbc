# NewHTBC
# Etape 1: 
il faut mettre en place l'environnement de php version 7.3 ou 7.4, de symfony et de composer pour que vous puissez lancer le projet.

# Etape 2:
Il faut avoir mon lien gitlab : https://gitlab.com/sabrine.ben/newhtbc/-/tree/develop/
Ensuit, il faut ouvrir une ligne de commande et cloner votre projet par la commande 'git clone https://gitlab.com/sabrine.ben/newhtbc/-/tree/develop/'.
Puis il faut ouvrir le projet dans un vs code avec la commande 'code .' .

# Etape 3:
Après que vous clonez le projet dans le système, il faut ouvrir un terminal et saisir la commande 'symfony console server:start' ou 'php bin/console server:run'.
Après que vous lancez le serveur vous cliquez sur le lien de localhost et vous peuvez faire le test. 
